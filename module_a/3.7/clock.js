const canvas_element = document.createElement("canvas");
const app_element = document.getElementById("app");
app_element.appendChild(canvas_element);
canvas_element.setAttribute("width", "1024px");
canvas_element.setAttribute("height", "1024px");
canvas_element.style.width = "500px"
canvas_element.style.height = "500px"
const ctx = canvas_element.getContext("2d");

var radius = canvas_element.height / 2;
ctx.translate(radius, radius);
radius = radius * 0.90

function drawClock() {
  drawFace(ctx, radius);
  drawTime(ctx, radius);
}

function drawFace(ctx, radius) {
  ctx.beginPath();
  ctx.arc(0, 0, radius, 0, 2*Math.PI);
  ctx.fillStyle = 'white';
  ctx.fill();
  ctx.drawImage(image_background_element, -500, -500);
}

function drawTime(ctx){
    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    //hour
    hour=hour%12;
    hour=(hour*Math.PI/6)+
    (minute*Math.PI/(6*60))+
    (second*Math.PI/(360*60));
    drawHand(ctx, hour, 250, 30);
    //minute
    minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
    drawHand(ctx, minute, 380, 30);
    // second
    second=(second*Math.PI/30);
    drawHandSecond(ctx, second);
}

function drawHand(ctx, pos, length, width) {
    ctx.beginPath();
    ctx.rotate(pos);
    ctx.fillStyle = "#222222";
    ctx.fillRect(-15, 50 - length, width, length);
    ctx.rotate(-pos);
}

function drawHandSecond(ctx, pos) {
    ctx.beginPath();
    ctx.rotate(pos);
    ctx.drawImage(image_second_line_element, -32, 70, image_second_line_element.width, -image_second_line_element.height);
    ctx.rotate(-pos);
}


const image_background_element =  new Image();
const image_second_line_element =  new Image();

const count_load_image = 2;
var status_load_image = 0;

image_background_element.onload = function(){
    status_load_image ++;
    runAction();
};

image_second_line_element.onload = function() {
    status_load_image ++;
    runAction();
}

function runAction() {
    if(status_load_image === 2) {
        setInterval(drawClock, 1000);
    }
}

image_background_element.src = './background.jpg';

image_second_line_element.src = './second_line.png'