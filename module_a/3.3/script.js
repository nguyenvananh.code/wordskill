const draw_app_element = document.getElementById("draw_app");
const ctx = draw_app_element.getContext("2d");

const width = window.innerWidth;
const height = window.innerHeight;


draw_app_element.width = width;
draw_app_element.height = height;

draw_app_element.onmousedown = startDrawing;
draw_app_element.onmouseup = stopDrawing;
draw_app_element.onmousemove = actionDrawing;

var object_draw = [];
var pulse = "";

const colors = ['red', 'green', 'blue'];

var color_select = colors[0];

function startDrawing(e) {
    one = ctx.getImageData(0, 0, draw_app_element.width, draw_app_element.height);
    iter(one);
    img = ctx.getImageData(0, 0, draw_app_element.width, draw_app_element.height);
    sX = e.x;
    sY = e.y;
    pulse = "on";
}

function iter(one) {
    if (object_draw.length < 10) {
        object_draw.push(one);
    } else {
        object_draw.shift();
        object_draw.push(one);
    }
}

function stopDrawing(event) {
    eX = event.x;
    eY = event.y;
    pulse = "off";
}

function actionDrawing(event) {
    mX = event.x;
    mY = event.y;
    if (pulse == "on") {
        draw();
    } else if (pulse == 'on') {
        ctx.putImageData(img, 0, 0);
        draw();
    }
}

function draw() {
    ctx.beginPath();
    ctx.lineWidth = 5;
    ctx.moveTo(sX, sY);
    ctx.lineTo(mX, mY);
    ctx.strokeStyle = color_select;
    ctx.stroke();
    sX = mX;
    sY = mY;
}