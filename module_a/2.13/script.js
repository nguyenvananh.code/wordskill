
// Tìm hiểu nguyên lý tại: https://en.wikipedia.org/wiki/Seven-segment_display

var digitSegments = [
    [1, 2, 3, 4, 5, 6],
    [2, 3],
    [1, 2, 7, 5, 4],
    [1, 2, 7, 3, 4],
    [6, 7, 2, 3],
    [1, 6, 7, 3, 4],
    [1, 6, 5, 4, 3, 7],
    [1, 2, 3],
    [1, 2, 3, 4, 5, 6, 7],
    [1, 2, 7, 3, 6]
]

document.addEventListener('DOMContentLoaded', function () {
    var _seconds = document.querySelectorAll('.seconds');
    var _centiseconds = document.querySelectorAll('.centiseconds');
    var max_centiseconds_number = 999 * 100 + 59;
    var _centiseconds_number = 0;
    var status = "pause";
    
    var btn_start_element = document.getElementById("btn_start_timer");
    var btn_stop_element = document.getElementById("btn_stop_timer");
    var btn_restart_element = document.getElementById("btn_restart_timer");

    var setIntervalAction = null;

    btn_start_element.addEventListener("click", function() {
        if(status !== "running") {
            startAction();
            status = "running";
        }
    });

    btn_stop_element.addEventListener("click", function() {
        if(status === "running") {
            clearInterval(setIntervalAction);
            setIntervalAction = null;
            status = "pause"
        }
    });


    btn_restart_element.addEventListener("click", function() {
        _centiseconds_number = 0;
        if(status !== "running") {
            setNumber(_seconds[0], 0, 1);
            setNumber(_seconds[1], 0, 1);
            setNumber(_seconds[2], 0, 1);
    
            setNumber(_centiseconds[0], 0, 1);
            setNumber(_centiseconds[1], 0, 1);
        }
    });
    

    var startAction = function() {
        setIntervalAction = setInterval(function () {
            if(_centiseconds_number > max_centiseconds_number) {
                clearInterval(setIntervalAction);
                setIntervalAction = null;
                status = "pause";
            } else {
                var centiseconds = _centiseconds_number % 100;
                var seconds = Math.floor(_centiseconds_number / 100);
        
                
                setNumber(_seconds[0], Math.floor(seconds / 100), 1);
                setNumber(_seconds[1], Math.floor(seconds / 10) % 10, 1);
                setNumber(_seconds[2], seconds % 10, 1);
        
                setNumber(_centiseconds[0], Math.floor(centiseconds / 10), 1);
                setNumber(_centiseconds[1], centiseconds % 10, 1);
                _centiseconds_number ++;
            }
        }, 10);
    };
});

var setNumber = function (digit, number, on) {
    var segments = digit.querySelectorAll('.segment');
    var current = parseInt(digit.getAttribute('data-value'));
    // only switch if number has changed or wasn't set
    if (!isNaN(current) && current != number) {
        // unset previous number
        digitSegments[current].forEach(function (digitSegment, index) {
            segments[digitSegment - 1].classList.remove('on');
        });
    }

    if (isNaN(current) || current != number) {
        // set new number after
        digitSegments[number].forEach(function (digitSegment, index) {
            segments[digitSegment - 1].classList.add('on');
        });
        digit.setAttribute('data-value', number);
    }
}