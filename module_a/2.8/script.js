const box_target_element = document.getElementById("box_target");

const inp_radius_css_element = document.getElementById("radius-inp");
const inp_shift_right_css_element = document.getElementById("shift-right-inp");
const inp_shift_down_css_element = document.getElementById("shift-down-inp");
const inp_spread_css_element = document.getElementById("spread-inp");
const inp_blur_css_element = document.getElementById("blur-inp");
const inp_color_css_element = document.getElementById("color-inp");
const inp_inset_css_element = document.getElementById("inset-inp");

inp_radius_css_element.addEventListener("change", function(event) {
    box_target_element.style.borderRadius = event.target.value + "px";
})


const changeStyleShadowBox = function() {
    var setup_css = `${inp_inset_css_element.checked ? 'inset' : ''} ${inp_shift_right_css_element.value}px ${inp_shift_down_css_element.value}px ${inp_blur_css_element.value}px ${inp_spread_css_element.value}px ${inp_color_css_element.value}`;
    box_target_element.style.boxShadow = setup_css;
}

inp_shift_right_css_element.addEventListener("change", changeStyleShadowBox);
inp_shift_down_css_element.addEventListener("change", changeStyleShadowBox);
inp_spread_css_element.addEventListener("change", changeStyleShadowBox);
inp_blur_css_element.addEventListener("change", changeStyleShadowBox);
inp_color_css_element.addEventListener("change", changeStyleShadowBox);
inp_inset_css_element.addEventListener("change", changeStyleShadowBox);

changeStyleShadowBox();
