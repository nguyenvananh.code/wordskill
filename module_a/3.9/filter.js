const canvas_element = document.getElementById("canvas_apps");
const select_img_elemnt = document.getElementById("select_img");
const select_filter_elemnt = document.getElementById("select_filter");

const ctx = canvas_element.getContext("2d");

const grap = 10;

var list_image_loading = [
    'athena.jpg',
    'mona-lisa.jpg',
    'theKiss.jpg',
    'young-pearl.jpg'
]

var status_load_image = 0;

var list_image_element = [];

var image_element_selected = null;

list_image_loading.forEach(item => {
    var image_element =  new Image();
    image_element.setAttribute("data-name-img", item)
    list_image_element.push(image_element);
    image_element.onload = function() {
        status_load_image ++;
        runAction();
    }
    image_element.src = "./imgs/" + item;
});

function runAction() {
    if(status_load_image === list_image_loading.length) {
        list_image_loading.forEach(item => {
            var option_img = document.createElement("option");
            option_img.innerText = item;
            option_img.setAttribute("value", item);
            select_img_elemnt.appendChild(option_img);
        });
        select_img_elemnt.removeAttribute("disabled");
        drawImageWithCanvas();
        select_img_elemnt.addEventListener("change", drawImageWithCanvas);
        select_filter_elemnt.addEventListener("change", addFilterWithImage);
    }
}

function getElementImage(name) {

    for (let i = 0; i < list_image_element.length; i++) {
        if(list_image_element[i].getAttribute('data-name-img') === name) {
            return list_image_element[i];
        }
    }
    return list_image_element[0];
}

function addFilterWithImage() {
    var value_filter_select = select_filter_elemnt.value;
    ctx.clearRect((canvas_element.width / 2) + grap, 0, (canvas_element.width / 2) - grap, canvas_element.height);
    if(image_element_selected !== null && value_filter_select.length > 0) {
        ctx.drawImage(image_element_selected,(canvas_element.width / 2) + grap, 0, (canvas_element.width / 2) - grap, canvas_element.height);
        // get raw pixel values
        var imageData = ctx.getImageData((canvas_element.width / 2) + grap, 0, (canvas_element.width / 2) - grap, canvas_element.height);
        var pixels = imageData.data;
        var value_change = 1;
        if(value_filter_select === 'darken') {
            value_change = 0.5;
        } else if(value_filter_select === 'lighten') {
            value_change = 1.5;
        }

        for(var i = 0; i < pixels.length; i += 4) {
            pixels[i] = pixels[i] * value_change;
            pixels[i+1] = pixels[i+1] * value_change;
            pixels[i+2] = pixels[i+2] * value_change;
        }
        ctx.putImageData(imageData, (canvas_element.width / 2) + grap, 0);
    }
}

function drawImageWithCanvas() {
    select_filter_elemnt.value = "";
    var value_image_select = select_img_elemnt.value;
    if(!value_image_select) value_image_select = "athena.jpg";
    image_element_selected = getElementImage(value_image_select);
    ctx.drawImage(image_element_selected, 0, 0, (canvas_element.width / 2) - grap, canvas_element.height);
    ctx.clearRect((canvas_element.width / 2) + grap, 0, (canvas_element.width / 2) - grap, canvas_element.height);
}