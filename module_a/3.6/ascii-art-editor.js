/**
 * JavaScript - ASCII Art Editor
 *
 * Your task is to implement all methods marked with @todo. You must not change any other method.
 * You may add as many methods to the ASCIIArtEditor class as you want.
 */


/**
 * Constructor function to create a new ASCIIArtEditor
 * @constructor
 */
var ASCIIArtEditor = function () {
    /**
     * When transforming images this property should be used as line
     * separator for all operations
     * @type {string}
     */
    this.lineSeperator = '\n';
};


/**
 * This function takes an arbitrary ASCII Art string as input and must
 * return a mirrored version of the given image.
 *
 * It should be possible to choose the mirror-axis with the second argument.
 *
 * The function should use the configured lineSeparator property on
 * the ASCIIArtEditor object.
 *
 * Example on 'x' axis:
 *   Input:                 Output:
 *     # --····-- $           # --====-- $
 *     #  +    -  $           #  +    -  $
 *     # --====-- $           # --····-- $
 *
 * Example on 'y' axis:
 *   Input:                 Output:
 *     # --····-- $           $ --····-- #
 *     #  +    -  $           $  -    +  #
 *     # --====-- $           $ --====-- #
 *
 * @param {string} image - the source image
 * @param {'x'|'y'} [axis='y'] - the axis to be used for the mirror operation, defaults to 'y'
 * @return string - the mirrored input image
 *
 * @throws Error - If an invalid axis was provided
 *
 * @todo
 */
ASCIIArtEditor.prototype.mirror = function (image, axis) {
    if (!axis) axis = 'y';
    switch (axis) {
        case 'x':
            var outs = [];
            image.split(this.lineSeperator).forEach(function (line) {
                outs.unshift(line)
            });
            return outs.join(this.lineSeperator);
        case 'y':
            var outs = [];
            image.split(this.lineSeperator).forEach(function (line) {
                var out = '';
                for (var i = line.length - 1; i >= 0; i -= 1) {
                    out += line[i];
                }
                outs.push(out)
            });
            return outs.join(this.lineSeperator);
        default:
            throw ("Error - If an invalid axis was provided");
    }
};


/**
 * Takes any SQUARE ASCII image and must rotate the image by the
 * given angle (only multiple of 90 allowed).
 *
 * The rotation should always be clockwise.
 *
 * Example:
 *   Input:    90deg:    180deg:    270deg:    360deg:
 *     #-*       $-#       *-$        ***         #-*
 *     --*       ---       *--        ---         --*
 *     $-*       ***       *-#        #-$         $-*
 *
 * @param {string} image
 * @param {number} angle, has to be one of 0, 90, 180, 270, 360
 * @return string
 *
 * @throws Error - If an invalid angle was provided
 *
 * @todo
 */
ASCIIArtEditor.prototype.rotate = function (image, angle) {
    var rotate90 = function (image, lineSeperator) {
        var columns = [];
        image.split(lineSeperator).forEach(function (line) {
            for (var i = 0; i < line.length; i += 1) {
                if(columns[i]) {
                    columns[i] = line[i] + columns[i];
                } else {
                    columns[i] = line[i];
                }
            }
        });
        return columns.join(lineSeperator)
    }
    angle = Number(angle);
    if (angle % 90 === 0 && angle >= 0 && angle <= 360) {
        for (let index = 1; index <= Math.floor(angle / 90); index++) {
            image = rotate90(image, this.lineSeperator);
        }
        return image;
    } else {
        throw ("Error - If an invalid axis was provided");
    }
};
