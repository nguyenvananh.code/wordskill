import { caller, getStorageData, setStorageData, deleteStorageData } from "./http";

export function getListCampaigns() {
    return new Promise((resolve) => {
        caller('GET', 'campaigns', {}).then(data => {
            resolve(data.campaigns);
        });
    });
}

export function getDetailCampaign(organizer_slug, campaign_slug) {
    return new Promise((resolve) => {
        caller('GET', `organizers/${organizer_slug}/campaigns/${campaign_slug}`, {}).then(data => {
            resolve(data);
        });
    });
}

export function getRegistrations() {
    return new Promise((resolve) => {
        caller('GET', `registrations`, {}).then(data => {
            resolve(data.registrations);
        });
    });
}

export function login(lastname, registration_code) {
    return new Promise((resolve, reject) => {
        caller('POST', `login`, {
            lastname: lastname,
            registration_code: registration_code
        }).then(data => {
            // Login done => set auth
            setStorageData('token_citizen', data.token);
            data.token = null;
            setStorageData('citizen', JSON.stringify(data));
            resolve(data);
        }).catch(data => {
            reject(data);
        });
    });
}

export function registrarion(organizer_slug, campaign_slug, data) {
    return new Promise((resolve, reject) => {
        caller('POST', `organizers/${organizer_slug}/campaigns/${campaign_slug}/registration`, data).then(data => {
            resolve(data);
        }).catch(data => {
            reject(data);
        });
    });
}

export function actionLogout() {
    return new Promise((resolve, reject) => {
        caller('POST', `logout`, {}).then(() => {
            deleteStorageData('token_citizen');
            deleteStorageData('citizen');
            resolve(true);
        }).catch(data => {
            reject(data);
        });
    });
}

export function getStatusLogin() {
    const token = getStorageData('token_citizen');
    return token !== null;
}

export function getUserLoginObj() {
    const statusLogin = getStatusLogin();
    const data = getStorageData('citizen');
    return {
        isLogin: statusLogin,
        obj: data ? JSON.parse(data) : false
    };
}