import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './views/Home.vue'
import Campaign from './views/Campaign.vue'
import Login from './views/Login.vue'
import NotFound from './views/NotFound.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { isLogin: false }
  },
  {
    path: '/:organizer_slug/:campaign_slug',
    name: 'Campaign',
    component: Campaign,
    meta: { isLogin: false }
  },
  {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { isLogin: true, loginStatus: false }
  },
  {
      path: '*',
      name: 'NotFound',
      component: NotFound,
      meta: { isLogin: false }
  },
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router