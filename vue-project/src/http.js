import CONFIG from '../config';

function addTokenToUrl(url) {
    url = CONFIG.domain + CONFIG.version + url;
    const token = getStorageData('token_citizen');
    if(url.indexOf('?') === -1) return url + "?token=" + token;
    return url + "&token=" + token;
}

export function getStorageData(name) {
    return window.localStorage.getItem(name);
}

export function deleteStorageData(name) {
    return window.localStorage.removeItem(name);
}

export function setStorageData(name, value) {
    return window.localStorage.setItem(name, value);
}

export function caller(method, url, data) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, addTokenToUrl(url), true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.addEventListener("loadend", (response) => {
            if(response.currentTarget.status === 200) {
                resolve(JSON.parse(response.currentTarget.response));
            } else {
                reject(JSON.parse(response.currentTarget.response))
            }
        });
        xhr.addEventListener("error", () => {
            
        });
        xhr.addEventListener("timeout", () => {
            
        });
        xhr.send(JSON.stringify(data));
    });
}