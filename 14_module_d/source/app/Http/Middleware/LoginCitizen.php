<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Citizen;

class LoginCitizen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->token;
        if($token) {
            $citizen = Citizen::where('login_token', $token)->count();
            if($citizen > 0) {
                return $next($request);
            }
        }
        return response()->json(["message" => "Invalid token"], 401);
    }
}
