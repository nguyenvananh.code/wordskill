<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SessionCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'type' => 'required',
            'place_id'  => 'required|exists:places,id',
            'description' => 'required',
            'vaccinator' => 'required',
            'start' => 'required|date_format:Y-m-d H:i',
            'end' => 'required|date_format:Y-m-d H:i|after:start'
        ];
    }


    public function messages()
    {
        return [
            'vaccinator.required' => 'Participant is required.',
            '*.required' => ":attribute is required."
        ];
    }
}
