<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaceCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'area_id' => 'required|exists:areas,id',
            'capacity' => 'required'
        ];
    }


    public function messages()
    {
        return [
            '*.required' => ":attribute is required."
        ];
    }
}
