<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Citizen;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if($request->lastname && $request->registration_code) {
            $citizen = Citizen::where('lastname', $request->lastname)->where('registration_code', $request->registration_code)->first();
            if($citizen) {
                if($citizen->login_token == null) {
                    $citizen->login_token = md5($citizen->username);
                    $citizen->save();
                }
                return response()->json([
                    'firstname' => $citizen->firstname,
                    'lastname' => $citizen->lastname,
                    'username' => $citizen->username,
                    'email' => $citizen->email,
                    'token' => $citizen->login_token,
                ]);
            }
        }
        return response()->json(["message" => "Invalid login"], 401);
    }

    public function logout(Request $request)
    {
        $citizen = Citizen::where('login_token', $request->token)->first();
        $citizen->login_token = null;
        $citizen->save();
        return response()->json(["message" => "Logout success"]);
    }
}
