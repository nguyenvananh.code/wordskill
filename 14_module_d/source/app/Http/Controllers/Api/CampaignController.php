<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\CampaignTicket;
use App\Models\Organizer;
use App\Models\Citizen;
use App\Models\Registration;
use App\Models\Session;
use App\Models\SessionRegistration;

class CampaignController extends Controller
{
    public function listCampaign()
    {
        return response()->json([
            "campaigns" => Campaign::with('organizer')->select('id', 'name', 'slug', 'organizer_id', 'date')->orderBy('date')->get()
        ]);
    }

    public function campaignDetail($organizer_slug, $campaign_slug)
    {
        $organizer = Organizer::where('slug', $organizer_slug)->first();
        if($organizer) {
            $campaign = Campaign::with('areas', 'tickets', 'areas.places', 'areas.places.sessions')->where('slug', $campaign_slug)->where('organizer_id', $organizer->id)->first();
            if($campaign) {
                return response()->json($campaign);
            }
            return response()->json(["message" => "Campaign not found"], 404);
        }
        return response()->json(["message" => "Organizer not found"], 404);
    }

    public function registration(Request $request)
    {
        $token = $request->token;
        if(!$token) return response()->json(["message" => "User not logged in"], 401);
        $citizen = Citizen::where('login_token', $token)->first();
        if(!$citizen) return response()->json(["message" => "User not logged in"], 401);
        $ticket = CampaignTicket::find($request->ticket_id);
        if(! is_array($request->session_ids)) {
            $session_ids = json_decode($request->session_ids, true);
        } else {
            $session_ids = $request->session_ids;
        }
        if(!$ticket) return response()->json(["message" => "Ticket not found"], 404);
        $sessions = Session::whereIn('id', $session_ids)->get();
        if(count($sessions) != count($session_ids)) return response()->json(["message" => "Sessions not found"], 404);
        $registration = Registration::where('ticket_id', $request->ticket_id)->where('citizen_id', $citizen->id)->count();
        if($registration > 0) return response()->json(["message" => "User already registered"], 401);
        if($ticket->available === false)  return response()->json(["message" => "Ticket is no longer available"], 401);
        $registration = new Registration;
        $registration->citizen_id = $citizen->id;
        $registration->ticket_id = $ticket->id;
        $registration->registration_time = now();
        $registration->save();
        foreach ($sessions as $session) {
            $session_registration = new SessionRegistration;
            $session_registration->registration_id = $registration->id;
            $session_registration->session_id = $session->id;
            $session_registration->save();
        }
        return response()->json(["message" => "Registration successful"], 200);
    }

    public function listRegistrations(Request $request)
    {
        $token = $request->token;
        if(!$token) return response()->json(["message" => "User not logged in"], 401);
        $citizen = Citizen::where('login_token', $token)->first();
        if(!$citizen) return response()->json(["message" => "User not logged in"], 401);
        $registrations = Registration::where('citizen_id', $citizen->id)->get();
        $results = [];
        foreach ($registrations as $registration) {
            $result = [
                "campaign" => Campaign::with('organizer')->select('id', 'name', 'slug', 'organizer_id', 'date')->where('id', $registration->ticket->campaign->id)->first(),
                "session_ids" => SessionRegistration::where('registration_id', $registration->id)->pluck('session_id')
            ];
            $results[] = $result;
        }
        return response()->json(["registrations" => $results], 200);
    }
}
