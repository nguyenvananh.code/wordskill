<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\CampaignCreateRequest;
use App\Http\Requests\CampaignEditRequest;
use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Auth;

class CampaignController extends Controller
{
    public function managers()
    {
        return view('campaigns.index');
    }

    public function createForm()
    {
        return view('campaigns.create');
    }

    public function actionCreate(CampaignCreateRequest $request)
    {
        $campaign = new Campaign;
        $campaign->fill($request->only($campaign->fillable));
        $campaign->organizer_id = Auth::user()->id;
        if($campaign->save()) {
            return redirect()->route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug])->with('suss', 'Campaign successfully created');
        }
        return redirect()->back()->with('err', 'Cant not create object');
    }

    public function detailCampaign($id, $slug)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        return view('campaigns.detail', compact('campaign', $campaign));
    }

    public function editCampaign($id, $slug)
    {
        $campaign = Campaign::where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        return view('campaigns.edit', compact('campaign', $campaign));
    }

    public function actionEdit($id, $slug, CampaignEditRequest $request)
    {
        $campaign = Campaign::where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        $campaign->fill($request->only($campaign->fillable));
        if($campaign->save()) {
            return redirect()->route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug])->with('suss', 'Campaign successfully updated');
        }
        return redirect()->back()->with('err', 'Cant not update object');
    }

    public function reportChart($id, $slug)
    {
        $campaign = Campaign::where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        $labels = [];
        $capacitys = [];
        $total_capacity = [];
        $color_capacitys = [];
        foreach ($campaign->places as $place) {
            $labels[] = $place->name;
            $capacitys[] = $place->capacity;
            $total_capacity[] = $place->session_registrations()->count();
            $color_capacitys[] = "#a1cbf5";
            if($place->capacity < $place->session_registrations()->count()) {
                $color_total_capacity[] = "#f1a8a2";
            } else {
                $color_total_capacity[] = "#cbdfac";
            }
        }
        return view('reports.index', compact('campaign', 'labels', 'capacitys', 'color_capacitys', 'total_capacity', 'color_total_capacity'));
    }
}
