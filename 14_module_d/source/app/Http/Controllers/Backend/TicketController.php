<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Auth;
use App\Http\Requests\TicketCreateRequest;
use App\Models\CampaignTicket;

class TicketController extends Controller
{
    public function createForm($id, $slug)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        return view('tickets.create', compact('campaign', $campaign));
    }
    public function actionCreate($id, $slug, TicketCreateRequest $request)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        // create type json:
        $ticket = new CampaignTicket;
        if($request->special_validity != null) {
            $special_validity = [];
            $special_validity['type'] = $request->special_validity;
            if($special_validity['type'] == 'amount') {
                if(! $request->special_validity) return redirect()->back()->with('err', 'Amount is required');
                $special_validity['amount'] = $request->amount;
            }
            if($special_validity['type'] == 'date') {
                if(! $request->valid_until) return redirect()->back()->with('err', 'Valid Until is required')->withInput();
                $special_validity['date'] = $request->valid_until;
            }
            $ticket->special_validity = json_encode($special_validity);
        }
        $ticket->name = $request->name;
        $ticket->cost = $request->cost;
        $ticket->campaign_id = $campaign->id;
        if($ticket->save()) {
            return redirect()->route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug])->with('suss', 'Ticket successfully created');
        }
        return redirect()->back()->with('err', 'Cant not update object');
    }
}
