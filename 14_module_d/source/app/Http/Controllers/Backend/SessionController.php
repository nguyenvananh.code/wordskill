<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Auth;
use App\Models\Area;
use App\Models\Place;
use App\Models\Session;
use App\Http\Requests\SessionCreateRequest;

class SessionController extends Controller
{
    public function createForm($id, $slug)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        return view('sessions.create', compact('campaign'));
    }

    public function actionCreate($id, $slug, SessionCreateRequest $request)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        $areas = Area::where('campaign_id', $campaign->id)->pluck('id');
        $place = Place::where('id', $request->place_id)->whereIn('area_id', $areas)->first();
        if(!$place) return redirect()->back()->with('err', 'Place select not avalibale')->withInput();
        $count_check_start = Session::where('place_id', $request->place_id)->where('start', '>=', $request->start)->where('start', '<=', $request->end)->count();
        $count_check_end = Session::where('place_id', $request->place_id)->where('end', '>=', $request->start)->where('end', '<=', $request->end)->count();
        if($count_check_start > 0 || $count_check_end > 0) return redirect()->back()->with('err', 'Places already booked during this time')->withInput();
        $session = new Session;
        $session->fill($request->only($session->fillable));
        if($session->save()) {
            return redirect()->route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug])->with('suss', 'Session successfully created');
        }
        return redirect()->back()->with('err', 'Cant not created object');
    }

    public function updateForm($id, $slug, $id_section)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        $areas = Area::where('campaign_id', $campaign->id)->pluck('id');
        $places = Place::whereIn('area_id', $areas)->pluck('id');
        $session = Session::where('id', $id_section)->whereIn('place_id', $places)->firstOrFail();
        return view('sessions.edit', compact('campaign', 'session'));
    }

    public function actionUpdate($id, $slug, $id_section,  SessionCreateRequest $request)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        $areas = Area::where('campaign_id', $campaign->id)->pluck('id');
        $places = Place::whereIn('area_id', $areas)->pluck('id');
        $session = Session::where('id', $id_section)->whereIn('place_id', $places)->firstOrFail();

        $areas = Area::where('campaign_id', $campaign->id)->pluck('id');
        $place = Place::where('id', $request->place_id)->whereIn('area_id', $areas)->first();
        if(!$place) return redirect()->back()->with('err', 'Place select not avalibale')->withInput();
        $count_check_start = Session::where('id', '<>', $session->id)->where('place_id', $request->place_id)->where('start', '>=', $request->start)->where('start', '<=', $request->end)->count();
        $count_check_end = Session::where('id', '<>', $session->id)->where('place_id', $request->place_id)->where('end', '>=', $request->start)->where('end', '<=', $request->end)->count();
        if($count_check_start > 0 || $count_check_end > 0) return redirect()->back()->with('err', 'Places already booked during this time')->withInput();
        $session->fill($request->only($session->fillable));
        if($session->save()) {
            return redirect()->route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug])->with('suss', 'Session successfully updated');
        }
        return redirect()->back()->with('err', 'Cant not created object');
    }
}
