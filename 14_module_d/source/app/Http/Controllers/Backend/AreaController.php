<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Auth;
use App\Models\Area;
use App\Http\Requests\AreaCreateRequest;

class AreaController extends Controller
{
    public function createForm($id, $slug)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        return view('areas.create', compact('campaign'));
    }

    public function actionCreate($id, $slug, AreaCreateRequest $request)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        $area = new Area;
        $area->fill($request->only($area->fillable));
        $area->campaign_id = $campaign->id;
        if($area->save()) {
            return redirect()->route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug])->with('suss', 'Area successfully created');
        }
        return redirect()->back()->with('err', 'Cant not create object');
    }
}
