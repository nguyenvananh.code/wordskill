<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use Auth;

class AuthController extends Controller
{

    public function getHome()
    {
        if(Auth::check()) return redirect()->route('campaign.manager');
        return redirect()->route('login');
    }

    public function loginForm()
    {
        if(Auth::check()) return redirect()->route('campaign.manager');
        return view('login');
    }

    public function actionLogin(AuthRequest $request)
    {
        if(Auth::attempt($request->only('email', 'password'))) {
            return redirect()->route('campaign.manager');
        }
        return redirect()->back()->with('error', 'Email or password not correct');
    }

    public function actionLogout(Request $request)
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
