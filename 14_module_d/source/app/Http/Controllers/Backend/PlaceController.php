<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Auth;
use App\Models\Place;
use App\Models\Area;
use App\Http\Requests\PlaceCreateRequest;

class PlaceController extends Controller
{
    public function createForm($id, $slug)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        return view('places.create', compact('campaign'));
    }

    public function actionCreate($id, $slug, PlaceCreateRequest $request)
    {
        $campaign = Campaign::with('tickets')->where('id', $id)->where('slug', $slug)->where('organizer_id', Auth::user()->id)->firstOrFail();
        $area = Area::where('id', $request->area_id)->where('campaign_id', $campaign->id)->first();
        if(!$area) return redirect()->back()->with('err', 'Area select not avalibale')->withInput();
        $place = new Place;
        $place->fill($request->only($place->fillable));
        if($place->save()) {
            return redirect()->route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug])->with('suss', 'Place successfully created');
        }
        return redirect()->back()->with('err', 'Cant not create object');
    }
}
