<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    public $timestamps = false;

    public $fillable = ['name'];

    public $hidden = ['campaign_id'];

    public function places()
    {
        return $this->hasMany(Place::class);
    }

    public function sessions()
    {
        return $this->hasManyThrough(Session::class, Place::class);
    }
}
