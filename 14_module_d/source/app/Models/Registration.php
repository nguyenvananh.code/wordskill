<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    public $timestamps = false;

    public function ticket()
    {
        return $this->belongsTo(CampaignTicket::class, 'ticket_id');
    }
}
