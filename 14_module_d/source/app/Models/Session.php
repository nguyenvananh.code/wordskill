<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    public $timestamps = false;

    public $fillable = ['title', 'type', 'place_id', 'cost', 'description', 'vaccinator', 'start', 'end'];

    public $hidden = ['place_id'];

    public $dates = ['start', 'end'];

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function getCostAttribute($value)
    {
        if($value == null) return 0;
        return (float)$value;
    }
}
