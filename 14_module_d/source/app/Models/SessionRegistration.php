<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SessionRegistration extends Model
{
    public $timestamps = false;

    public $table = "session_registrations";
}
