<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    public $timestamps = false;

    public $hidden = ['organizer_id'];

    // public $dates = ['date'];

    public $fillable = ['name', 'slug', 'date'];

    public function getDateAttribute( $value ) {
        return (\Carbon\Carbon::parse($value))->format('Y-m-d');
    }

    public function registrations()
    {
        return $this->hasManyThrough(Registration::class, CampaignTicket::class, 'campaign_id' , 'ticket_id');
    }

    public function tickets()
    {
        return $this->hasMany(CampaignTicket::class);
    }

    public function areas()
    {
        return $this->hasMany(Area::class);
    }

    public function places()
    {
        return $this->hasManyThrough(Place::class, Area::class);
    }

    public function sessions()
    {
        return Session::whereIn('place_id', $this->places->pluck('id'))->get();
    }

    public function organizer()
    {
        return $this->belongsTo(Organizer::class);
    }
}
