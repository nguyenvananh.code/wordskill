<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    public $timestamps = false;

    public $fillable = ['name', 'area_id', 'capacity'];


    public $hidden = ['area_id', 'capacity'];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function session_registrations()
    {
        return $this->hasManyThrough(SessionRegistration::class, Session::class);
    }

    public function sessions()
    {
        return $this->hasMany(Session::class);
    }
}
