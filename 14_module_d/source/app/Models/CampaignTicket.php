<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignTicket extends Model
{
    public $timestamps = false;

    protected $appends = ['description', 'available'];

    public $hidden = ['campaign_id', 'special_validity'];

    public function getSpecialValidityAttribute($value)
    {
        $jsonObj = $value ? json_decode($value, true) : [];
        if(count($jsonObj) > 0) {
            $this->attributes['available'] = true;
            if($jsonObj['type'] == 'amount') {
                return $jsonObj['amount'] . ' tickets available';
            } else {
                $date_time = \Carbon\Carbon::parse($jsonObj['date']);
                return 'Available until ' . $date_time->format("F ") . $date_time->day . ", ". $date_time->year;
            }
        } else {
            return null;
        }
    }

    public function getDescriptionAttribute()
    {
        if($this->special_validity != null) {
            return $this->special_validity;
        }
        return null;
    }

    public function getAvailableAttribute()
    {
        $jsonObj = $this->attributes['special_validity'] ? json_decode($this->attributes['special_validity'], true) : [];
        if(count($jsonObj) > 0) {
            $this->attributes['available'] = true;
            if($jsonObj['type'] == 'amount') {
                $count_registration = $this->registrations()->count();
                return (int)$jsonObj['amount'] - $count_registration > 0;
            } else {
                $time = \Carbon\Carbon::parse($jsonObj['date'])->timestamp;
                $now = now()->timestamp;
                return $time - $now >= 0;
            }
        } else {
            return true;
        }
    }


    public function getCostAttribute($value)
    {
        if($value == null) return 0;
        return (float)$value;
    }

    public function registrations()
    {
        return $this->hasMany(Registration::class, 'ticket_id');
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }
}
