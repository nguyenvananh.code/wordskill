<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Backend\AuthController@getHome');


Route::get('/login', 'Backend\AuthController@loginForm')->name('login');
Route::post('/login', 'Backend\AuthController@actionLogin')->name('login');

Route::middleware('auth')->group(function () {
    Route::get('/logout', 'Backend\AuthController@actionLogout')->name('logout');
    Route::prefix('campaign')->name('campaign.')->group(function () {
        Route::get('/', 'Backend\CampaignController@managers')->name('manager');
        Route::get('/create', 'Backend\CampaignController@createForm')->name('create');
        Route::post('/create', 'Backend\CampaignController@actionCreate')->name('create');
        Route::get('/{id}-{slug}', 'Backend\CampaignController@detailCampaign')->name('detail');
        Route::get('/{id}-{slug}/edit', 'Backend\CampaignController@editCampaign')->name('edit');
        Route::post('/{id}-{slug}/edit', 'Backend\CampaignController@actionEdit')->name('edit');
        Route::get('/{id}-{slug}/create-ticket', 'Backend\TicketController@createForm')->name('create_ticket');
        Route::post('/{id}-{slug}/create-ticket', 'Backend\TicketController@actionCreate')->name('create_ticket');
        Route::get('/{id}-{slug}/create-area', 'Backend\AreaController@createForm')->name('create_area');
        Route::post('/{id}-{slug}/create-area', 'Backend\AreaController@actionCreate')->name('create_area');
        Route::get('/{id}-{slug}/create-place', 'Backend\PlaceController@createForm')->name('create_place');
        Route::post('/{id}-{slug}/create-place', 'Backend\PlaceController@actionCreate')->name('create_place');
        Route::get('/{id}-{slug}/report', 'Backend\CampaignController@reportChart')->name('report');


        Route::get('/{id}-{slug}/create-session', 'Backend\SessionController@createForm')->name('create_session');
        Route::post('/{id}-{slug}/create-session', 'Backend\SessionController@actionCreate')->name('create_session');


        Route::get('/{id}-{slug}/update-session/{id_session}', 'Backend\SessionController@updateForm')->name('update_session');
        Route::post('/{id}-{slug}/update-session/{id_session}', 'Backend\SessionController@actionUpdate')->name('update_session');
    });
});
