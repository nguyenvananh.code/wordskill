<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function() {
    Route::get('campaigns', 'Api\CampaignController@listCampaign');

    Route::get('organizers/{organizer_slug}/campaigns/{campaign_slug}', 'Api\CampaignController@campaignDetail');

    Route::get('registrations', 'Api\CampaignController@listRegistrations');

    Route::post('organizers/{organizer_slug}/campaigns/{campaign_slug}/registration', 'Api\CampaignController@registration');

    Route::post('login', 'Api\AuthController@login');

    Route::post('logout', 'Api\AuthController@logout')->middleware('citizen');
});
