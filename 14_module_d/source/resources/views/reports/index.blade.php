<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CVC Backend</title>

    <base href="{{ asset('/') }}">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="chart/Chart.min.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('campaign.manager')}}">CVC Backend</a>
    <span class="navbar-organizer w-100">{{ Auth::user()->name }}</span>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" id="logout" href="{{ route('logout') }}">Sign out</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item"><a class="nav-link" href="{{ route('campaign.manager')}}">Manage Campaigns</a></li>
                </ul>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>{{ $campaign->name }}</span>
                </h6>
                <ul class="nav flex-column">
                    <li class="nav-item"><a class="nav-link" href="{{ route('campaign.detail', [$campaign->id, $campaign->slug]) }}">Overview</a></li>
                </ul>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>Reports</span>
                </h6>
                <ul class="nav flex-column mb-2">
                    <li class="nav-item"><a class="nav-link active" href="{{ route('campaign.report', ['id' => $campaign->id, 'slug' => $campaign->slug]) }}">Place capacity</a></li>
                </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="border-bottom mb-3 pt-3 pb-2">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h2">{{ $campaign->name }}</h1>
                </div>
                <span class="h6">{{ $campaign->date }}</span>
            </div>

            <div class="mb-3 pt-3 pb-2">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h2 class="h4">Place Capacity</h2>
                </div>
            </div>

            <!-- TODO create chart here -->
            <canvas id="chart"></canvas>
        </main>
    </div>
</div>
<script src="chart/Chart.min.js"></script>
<script>
const chart = document.getElementById('chart');
var ctx = chart.getContext('2d');
chart.height = 100;
var myBarChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: {!! json_encode($labels) !!},
        datasets: [
            {
                "label": "Citizen",
                "data": {!! json_encode($total_capacity) !!},
                "fill": false,
                "backgroundColor": {!! json_encode($color_total_capacity) !!},
                "borderColor": {!! json_encode($color_total_capacity) !!},
                "borderWidth": 1
            },
            {
                "label": "Capacity",
                "data": {!! json_encode($capacitys) !!},
                "fill": false,
                "backgroundColor": {!! json_encode($color_capacitys) !!},
                "borderColor": {!! json_encode($color_capacitys) !!},
                "borderWidth": 1
            }
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Capacity'
                },
                gridLines: {
                    display: false
                },
            }],
            xAxes: [{
            }]
        },
        legend: {
            position: 'right'
        }
    }
});
</script>
</body>
</html>
