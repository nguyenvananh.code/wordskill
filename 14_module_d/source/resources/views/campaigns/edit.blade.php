<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CVC Backend</title>

    <base href="../../">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles -->
    <link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('campaign.manager')}}">CVC Backend</a>
    <span class="navbar-organizer w-100">{{ Auth::user()->name }}</span>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" id="logout" href="{{ route('logout') }}">Sign out</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item"><a class="nav-link" href="{{ route('campaign.manager')}}">Manage Campaigns</a></li>
                </ul>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>{{ $campaign->name }}</span>
                </h6>
                <ul class="nav flex-column">
                    <li class="nav-item"><a class="nav-link active" href="{{ route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug]) }}">Overview</a></li>
                </ul>

                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>Reports</span>
                </h6>
                <ul class="nav flex-column mb-2">
                    <li class="nav-item"><a class="nav-link" href="{{ route('campaign.report', ['id' => $campaign->id, 'slug' => $campaign->slug]) }}">Place capacity</a></li>
                </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="border-bottom mb-3 pt-3 pb-2">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h2">{{ $campaign->name }}</h1>
                </div>
            </div>

            <form class="needs-validation" novalidate action="{{ route('campaign.edit', ['id' => $campaign->id, 'slug' => $campaign->slug]) }}" method="POST">
                @include('components.showNoti')
                @csrf
                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        @component('components.input', ['errors' => $errors, 'label' => 'name'])
                        <label for="inputName">Name</label>
                        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="inputName" name="name" placeholder="" value="{{ old('name') ? old('name') : $campaign->name }}">
                        @endcomponent
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        @component('components.input', ['errors' => $errors, 'label' => 'slug'])
                        <label for="inputSlug">Slug</label>
                        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="inputSlug" name="slug" placeholder="" value="{{ old('slug') ? old('slug') : $campaign->slug }}">
                        @endcomponent
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        @component('components.input', ['errors' => $errors, 'label' => 'date'])
                        <label for="inputDate">Date</label>
                        <input type="text"
                               class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                               id="inputDate"
                               name="date"
                               placeholder="yyyy-mm-dd"
                               value="{{ old('date') ? old('date') : $campaign->date }}">
                        @endcomponent
                    </div>
                </div>

                <hr class="mb-4">
                <button class="btn btn-primary" type="submit">Save</button>
                <a href="{{ route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug]) }}" class="btn btn-link">Cancel</a>
            </form>

        </main>
    </div>
</div>

</body>
</html>
