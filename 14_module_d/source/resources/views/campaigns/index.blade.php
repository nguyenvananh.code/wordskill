<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CVC Backend</title>

    <base href="{{ asset('/') }}">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles -->
    <link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('campaign.manager') }}">CVC Platform</a>
    <span class="navbar-organizer w-100">{{ Auth::user()->name }}</span>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" id="logout" href="{{ route('logout') }}">Sign out</a>
        </li>
    </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item"><a class="nav-link active" href="{{ route('campaign.manager') }}">Manage Campaigns</a></li>
                </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Manage Campaigns</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <a href="{{ route('campaign.create') }}" class="btn btn-sm btn-outline-secondary">Create new campaigns</a>
                    </div>
                </div>
            </div>

            <div class="row campaign">
                @foreach(Auth::user()->campaigns as $campaign)
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <a href="{{ route('campaign.detail', ['id' => $campaign->id, 'slug' => $campaign->slug]) }}" class="btn text-left campaign">
                            <div class="card-body">
                                <h5 class="card-title">{{ $campaign->name }}</h5>
                                <p class="card-subtitle">{{ $campaign->date }}</p>
                                <hr>
                                <p class="card-text">{{ number_format($campaign->registrations()->count()) }} registrations</p>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>

        </main>
    </div>
</div>

</body>
</html>
