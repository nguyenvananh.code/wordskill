{{ $slot }}
@if($errors->has($label))
<div class="invalid-feedback" style="text-transform: capitalize;">
    {{ $errors->first($label) }}
</div>
@endif
