@if(session()->has('err'))
    <div class="alert alert-danger">
        {{ session()->get('err') }}
    </div>
@endif

@if(session()->has('suss'))
    <div class="alert alert-success">
        {{ session()->get('suss') }}
    </div>
@endif
