<?php

use Illuminate\Database\Seeder;
use App\Models\Organizer;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Seed data with Organizer
        $user = Organizer::find(1);
        $user->password_hash = Hash::make('demopass1');
        $user->save();
        $user = Organizer::find(2);
        $user->password_hash = Hash::make('demopass2');
        $user->save();
    }
}
